#!/usr/bin/bash

NAME="snappy-java"
VERSION=$(rpmspec -q $NAME.spec --srpm --qf "%{version}")

wget https://github.com/xerial/$NAME/archive/$VERSION.tar.gz

tar -xzf $VERSION.tar.gz

pushd $NAME-$VERSION

# remove proprietary files
rm lib/inc_ibm/jni_md.h
rm lib/inc_ibm/jniport.h
rmdir lib/inc_ibm
rm lib/inc_mac/jni_md.h
rm lib/inc_mac/jni.h
rmdir lib/inc_mac
rm lib/inc_win/jni.h
rm lib/inc_win/jni_md.h
rmdir lib/inc_win
rm lib/inc_linux/jni_md.h
rm lib/inc_linux/jni.h
rmdir lib/inc_linux

# remove precompiled class
rm lib/org/xerial/snappy/OSInfo.class
rmdir lib/org/xerial/snappy
rmdir lib/org/xerial
rmdir lib/org

popd

tar -czf $NAME-$VERSION-clean.tar.gz $NAME-$VERSION
rm -rf $NAME-$VERSION
rm $VERSION.tar.gz

